using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading.Channels;

namespace AssignmentThree;

public class convert
{
    public static void convert24bitTo8bit(Bitmap bmp)
    {
        Bitmap newImage = new Bitmap(bmp.Width, bmp.Height, PixelFormat.Format8bppIndexed);
        ColorPalette palette = newImage.Palette;
        
        for (int i = 0; i < 256; i++)
        {

            palette.Entries[i] = Color.FromArgb(i, i, i);
        } 
        newImage.Palette = palette; 
        
        Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
        BitmapData bmpData = bmp.LockBits(rect, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
        BitmapData newData = newImage.LockBits(rect, ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);
        
        
        int originalStride = bmpData.Stride;
        int newStride = newData.Stride;
        IntPtr originalPtr = bmpData.Scan0;
        IntPtr newPtr = newData.Scan0;
        
        unsafe
        {
            
            int nwidth = bmp.Width;
            int nHeigth = bmp.Height;
            for (int y = 0; y < nHeigth; y++)
            {
                
                byte * originalRow = (byte *)originalPtr + y * originalStride;  
                byte * newRow = (byte *)newPtr + y * newStride;
                
                for (int x = 0; x < nwidth; x++)
                {
                   
                    byte r = originalRow[x * 3 +2];
                    byte g = originalRow[x * 3 + 1];
                    byte b = originalRow[x * 3 ];
                    byte newValue = (byte)(byte)((.299 * r) + (.587 * g )+ (.114 * b));
                    newRow[x] = newValue;
                }

            }
        }
          bmp.UnlockBits(bmpData);
          newImage.UnlockBits(newData);
          
          newImage.Save("../../../Images/imageConvert8bit.jpg");

    }
}
